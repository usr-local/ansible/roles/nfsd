nfsd
====

Install (if needed) the nfsd daemon and configure the `/etc/exports` file.

Primarily intended for OpenBSD but supports Debian as well.

Requirements
------------

N/A

Role Variables
--------------

`.0.*` entries are examples of the first item in the aforementioned list.

| Variable              | Default | Example         | Description                                                                          |
|-----------------------|---------|-----------------|--------------------------------------------------------------------------------------|
| `nfsd_root_group`     | `wheel` | `root`          | Primary group of the `root` user; `OpenBSD` uses `wheel` but some Linuxes use `root` |
| `exported_fs`         | `[]`    | `See below`     | List of paths and hosts allowed to access said paths                                 |
| `.0.path`             | `N/A`   | `/data`         | Exported path                                                                        |
| `.0.options`          | `N/A`   | `-mapall=_user` | Optional export options                                                              |
| `.0.allowed`          | `[]`    | `See below`     | List of hosts and associated options                                                 |
| `.0.allowed.[0].item` | `N/A`   | `host1(sync)`   | Entry in the list above                                                              |

### Examples ###

```
exported_fs:
  - path: /data
    options: "-mapall=_unprivileged_user"
    allowed:
      - "fred(sync,no_subtree_check)"
      - "barney(ro,sync,no_subtree_check)"
```

Dependencies
------------

N/A

Example Playbook
----------------

Suppose one uses a `bedrock.ini` inventory file that contains these hosts:

    [all]
    bedroom-pc.bedrock.lan
    living-pc.bedrock.lan

and a `manage_nfsd.yml` playbook that looks like this:

    - hosts: all
      roles:
         - role: nfsd

one could call ansible like this:

    $ ansible-playbook -i bedrock.ini manage_nfsd.yml

License
-------

BSD

Author Information
------------------

[psa90](https://gitlab.com/psa90)
